'''
Created on Feb 7, 2018

@author: yaswanth
'''
import requests
from urlparse import urljoin 
import json
from StringIO import StringIO
from _ast import Dict
import os

class CVESearch(object):
 
    def __init__(self, base_url='https://cve.circl.lu', proxies=None):
        self.base_url = base_url
        self.session = requests.Session()
        self.session.proxies = proxies
        self.session.headers.update({
            'content-type': 'application/json',
            'User-Agent': 'PyCVESearch - python wrapper'})
        
    def _http_get(self, api_call, query=None):
        response = self.session.get('https://cve.circl.lu/cve/CVE-2017-15103')
        return(response)
    
    def search(self, param):
        """ search() returns a dict containing all the vulnerabilities per
            vendor and a specific product
        """
        data = self._http_get('cve', query=param)
        return (data)
    
from HTMLParser import HTMLParser

class MyHTMLParser(HTMLParser):
    tag_values = list()
    tag =None
    value =''
    
    
    def handle_starttag(self, tag, attrs):
        self.tag = tag 
#    
    def handle_endtag(self, tag):
        if self.tag == tag and  self.value: 
            self.tag_values.append( self.tag + ':' + self.value)
        self.value =''
    def handle_data(self, data):
        if 'td' in self.tag or 'b' in self.tag or 'span' in self.tag: 
                self.value = ''.join(self.tag_values)
                print(self.tag,data)    

if __name__== "__main__":
    print("main")    
    p1 = CVESearch()
    data=p1.search('CVE-2017-15103')
    print(data.content)
    parser = MyHTMLParser()
    parser.feed(data.content)
    print(parser.tag_values)